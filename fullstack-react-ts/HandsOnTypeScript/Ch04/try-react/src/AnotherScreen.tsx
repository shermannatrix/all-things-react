import React, { FC } from "react";

const AnotherScreen = () => {
	return <div>Hello World! Another Screen</div>;
};

export default AnotherScreen;