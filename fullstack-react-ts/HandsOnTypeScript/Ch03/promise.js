const myPromise = new Promise((resolve, reject) => {
	setTimeout(() => {
		// reject('I failed');
		resolve('I completed successfully');
	}, 500);
});

myPromise.then(done => {
	console.log(done);
}).catch(err => {
	console.log(err);
});