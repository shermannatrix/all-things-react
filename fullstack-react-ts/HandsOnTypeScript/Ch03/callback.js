function letmeKnowWhenComplete(size, callback) {
	var reducer = 0;
	for (var i = 1; i < size; i++) {
		reducer = Math.sin(reducer * i);
	}
	callback();
}

letmeKnowWhenComplete(10000000, function() {
	console.log('Great! It completed!');
});