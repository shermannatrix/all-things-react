import { useState, useEffect } from 'react';
import { createConnection } from './chat.js';
import './App.css';

function App() {
	useEffect(() => {
		const connection = createConnection();
		connection.connect();
		return () => connection.disconnect();
	}, []);

	return (
		<div className="App">
			<h1>Welcome to the chat!</h1>
		</div>
	);
}

export default App;
