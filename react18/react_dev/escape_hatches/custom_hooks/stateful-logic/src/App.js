import { useFormInput } from './useFormInput';
import './App.css';

function Form() {
	const firstNameProps = useFormInput('Mary');
	const lastNameProps = useFormInput('Poppins');

	return (
		<>
			<label>
				First name:
				<input {...firstNameProps} />
			</label>
			<label>
				Last name:
				<input {...lastNameProps} />
			</label>
			<p><b>Good morning, {firstNameProps.value} {lastNameProps.value}.</b></p>
		</>
	);
}

function App() {
	return (
		<div className="App">
			<Form />
		</div>
	);
}

export default App;
