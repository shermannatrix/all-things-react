import { useOnlineStatus } from './useOnlineStatus';
import './App.css';

function StatusBar() {
	const isOnline = useOnlineStatus();
	return <h1>{isOnline ? '✅ Online' : '❌ Disconnected'}</h1>;
}

function SaveButton() {
	const isOnline = useOnlineStatus();

	function handleSaveClick() {
		console.log('✅ Progress saved');
	}

	return (
		<button disabled={!isOnline} onClick={handleSaveClick}>
			{isOnline ? 'Save progress' : 'Reconnecting...'}
		</button>
	);
}

function App() {
	return (
		<div className="App">
			<SaveButton />
			<StatusBar />
		</div>
	);
}

export default App;
