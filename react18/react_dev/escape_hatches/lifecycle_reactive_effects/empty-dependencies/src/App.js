import { useState, useEffect } from 'react';
import { createConnection } from './chat';
import './App.css';

const serverUrl = 'https://localhost:1234';
const roomId = 'general';

function ChatRoom() {
	useEffect(() => {
		const connection = createConnection(serverUrl, roomId);
		connection.connect();
		return () => connection.disconnect();
	}, []);
	return <h1>Welcome to the {roomId} room!</h1>;
}

function App() {
	const [show, setShow] = useState(false);
	return (
		<div className="App">
			<button onClick={() => setShow(!show)}>
				{show ? 'Close chat' : 'Open chat'}
			</button>
			{show && <hr />}
			{show && <ChatRoom />}
		</div>
	);
}

export default App;
