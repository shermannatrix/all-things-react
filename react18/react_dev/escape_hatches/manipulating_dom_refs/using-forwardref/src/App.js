import { forwardRef, useRef } from 'react';
import './App.css';

const MyInput = forwardRef((props, ref) => {
	return <input {...props} ref={ref} />;
});

function App() {
	const inputRef = useRef(null);

	function handleClick() {
		inputRef.current.focus();
	}

	return (
		<div className="App">
			<MyInput ref={inputRef} />
			<button onClick={handleClick}>
				Focus the input
			</button>
		</div>
	);
}

export default App;
