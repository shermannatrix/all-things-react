import { useState, useRef } from 'react';
import './App.css';

function App() {
	const [show, setShow] = useState(true);
	const ref = useRef(null);

	return (
		<div className="App">
			<button onClick={() => {
				setShow(!show);
			}}>
				Toggle with setState
			</button>
			<button onClick={() => {
				ref.current.remove();
			}}>
				Remove from the DOM
			</button>
			{show && <p ref={ref}>Hello world</p>}
		</div>
	);
}

export default App;
