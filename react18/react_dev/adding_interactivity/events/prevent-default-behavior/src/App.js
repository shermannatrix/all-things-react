import logo from './logo.svg';
import './App.css';

function Signup() {
	return (
		<form onSubmit={e => {
			e.preventDefault();
			alert('Submitting!');
		}}>
			<input />
			<button>Send</button>
		</form>
	);
}

function App() {
	return (
		<div className="App">
			<Signup />
		</div>
	);
}

export default App;
