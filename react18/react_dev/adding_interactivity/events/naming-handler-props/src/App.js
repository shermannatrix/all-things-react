import logo from './logo.svg';
import './App.css';

function Button({ onSmash, children }) {
	return (
		<button onClick={onSmash}>
			{children}
		</button>
	);
}

function App() {
	return (
		<div className="App">
			<Button onSmash={() => alert('Playing!')}>
				Play movie
			</Button>
			<Button onSmash={() => alert('Uploading!')}>
				Upload Image
			</Button>
		</div>
	);
}

export default App;
