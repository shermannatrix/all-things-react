import './App.css';

function App() {
	function handleClick() {
		alert('You clicked me!');
	}
	return (
		/* <button onClick={handleClick}>
			Click me
		</button> */
		// Alternatively, we can have an inline handler
		<button onClick={() => {
			alert('You clicked me!');
		}}>
			Click me
		</button>
	);
}

export default App;
