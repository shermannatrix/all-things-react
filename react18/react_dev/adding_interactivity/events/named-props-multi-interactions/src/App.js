import logo from './logo.svg';
import './App.css';

function Button({ onClick, children }) {
	return (
		<button onClick={onClick}>
			{children}
		</button>
	);
}

function Toolbar({ onPlayMovie, onUploadImage }) {
	return (
		<div>
			<Button onClick={onPlayMovie}>
				Play Movie
			</Button>
			<Button onClick={onUploadImage}>
				Upload Image
			</Button>
		</div>
	);
}

function App() {
	return (
		<div className="App">
			<Toolbar
				onPlayMovie={() => alert('Playing!')}
				onUploadImage={() => alert('Uploading!')} />
		</div>
	);
}

export default App;
