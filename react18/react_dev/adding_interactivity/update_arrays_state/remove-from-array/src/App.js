import { useState } from 'react';
import './App.css';

let initialArtists = [
	{ id: 0, name: 'Martin Colvin Andrade' },
	{ id: 1, name: 'Lamidi Olonade Fakeye' },
	{ id: 2, name: 'Louise Nevelson' }
];

function App() {
	const [artists, setArtists] = useState(initialArtists);
	return (
		<div className="App">
			<h1>Inspiring sculptors:</h1>
			<ul>
				{artists.map(artist => (
					<li key={artist.id}>
						{artist.name}{' '}
						<button onClick={() => {
							setArtists(
								artists.filter(a => a.id !== artist.id)
							);
						}}>Delete</button>
					</li>
				))}
			</ul>
		</div>
	);
}

export default App;
