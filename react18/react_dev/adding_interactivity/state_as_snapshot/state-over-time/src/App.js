import { useState } from 'react';

function App() {
	const [number, setNumber] = useState(0);

	return (
		<div className="App">
			<h1>{number}</h1>
			<button onClick={() => {
				setNumber(number + 5);
				setTimeout(() => {
					alert(number);
				}, 3000);
			}}>+5</button>
		</div>
	);
}

export default App;
