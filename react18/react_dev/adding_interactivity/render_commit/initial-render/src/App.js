import './App.css';
import Image from './Image.js';

function App() {
  return (
    <div className="App">
      <Image />
    </div>
  );
}

export default App;
