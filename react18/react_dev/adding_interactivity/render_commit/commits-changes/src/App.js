import { useState, useEffect } from 'react';
import './App.css';

function useTime() {
	const [time, setTime] = useState(() => new Date());
	useEffect(() => {
		const id = setInterval(() => {
			setTime(new Date());
		}, 1000);
		return () => clearInterval(id);
	}, []);
	return time;
}

function App() {
	const time = useTime();
	return (
		<div className="App">
			<h1>{time.toLocaleTimeString()}</h1>
			<input />
		</div>
	);
}

export default App;
