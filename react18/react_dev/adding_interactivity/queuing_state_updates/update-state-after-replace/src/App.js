import { useState } from 'react';
import './App.css';

function App() {
	const [number, setNumber] = useState(0);
	return (
		<div className="App">
			<h1>{number}</h1>
			<button onClick={() => {
				setNumber(number + 5);
				setNumber(n => n + 1);
			}}>Increase the number</button>
		</div>
	);
}

export default App;
