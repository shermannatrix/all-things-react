import { useState } from 'react';
import './App.css';

function App() {
	const [number, setNumber] = useState(0);

	return (
		<div className="App">
			<h1>{number}</h1>
			<button onClick={() => {
				setNumber(n => n + 1);
				setNumber(n => n + 1);
				setNumber(n => n + 1);
			}}>+3</button>		
		</div>
	);
}

export default App;
