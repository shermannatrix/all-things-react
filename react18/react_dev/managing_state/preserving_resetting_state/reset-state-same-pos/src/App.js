import { useState } from 'react';
import './App.css';

function Counter({ person }) {
	const [score, setScore] = useState(0);
	const [hover, setHover] = useState(false);

	let className = 'counter';
	if (hover) {
		className += ' hover';
	}

	return (
		<div
			className={className}
			onPointerEnter={() => setHover(true)}
			onPointerLeave={() => setHover(false)}
		>
			<h1>{person}'s score: {score}</h1>
			<button onClick={() => setScore(score + 1)}>
				Add one
			</button>
		</div>
	);
}

function Scoreboard() {
	const [isPlayerA, setIsPlayerA] = useState(true);
	return (
		<div>
			{/* (Option 1) We can render a component in different positions. */}
			{/* {isPlayerA &&
				<Counter person="Taylor" />
			}
			{!isPlayerA &&
				<Counter person="Sarah" />
			} */}
			{/* (Option 2) We can reset state with a key. */}
			{isPlayerA ? (
				<Counter key="Taylor" person="Taylor" />
			) : (
				<Counter key="Sarah" person="Sarah" />
			)}

			{/* {isPlayerA ? (
				<Counter person="Taylor" />
			) : (
				<Counter person="Sarah" />
			)} */}
			<button onClick={() => {
				setIsPlayerA(!isPlayerA);
			}}>
				Next Player!
			</button>
		</div>
	);
}

function App() {
	return (
		<div className="App">
			<Scoreboard />
		</div>
	);
}

export default App;
