import AddTask from './AddTask';
import TaskList from './TaskList';
import { TasksProvider } from './TasksContext';
import './App.css';

function App() {
  return (
    <div className="App">
      <TasksProvider>
        <h1>Day off in Kyoto</h1>
        <AddTask />
        <TaskList />
      </TasksProvider>
    </div>
  );
}

export default App;
