import './App.css';

const people = [
	'Creola Katherine Johnson: mathematician',
	'Mario Jose Molina-Pasquel Henriquez: chemist',
	'Mohammad Abdus Salam: physicist',
	'Percy Lavon Julian: chemist',
	'Subrahmanyan Chandrasekhar: astrophysicist'
];

function List() {
	const listItems = people.map(person => <li>{person}</li>);
	return <ul>{listItems}</ul>;
}

function App() {
	return (
		<List />
	);
}

export default App;
