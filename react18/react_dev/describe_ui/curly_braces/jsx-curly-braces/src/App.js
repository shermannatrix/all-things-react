import logo from "./logo.svg";
import "./App.css";

function App() {
    const avatar = "https://i.imgur.com/7vQD0fPs.jpg";
    const description = "Gregorio Y. Zara";
    return <img className="avatar" src={avatar} alt={description} />;
}

export default App;
