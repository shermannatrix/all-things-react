function Item({ name, isPacked }) {
  // using the Logical AND operator (&&)
  return (
    <li className="item">
      {name} {isPacked && '✔'}
    </li>
  );

  // if (isPacked) {
  //   // return <li className="item">{name} ✔</li>\
  //   return null;
  // }
  // return <li className="item">{name}</li>

  // using a conditional (ternary) operator
  // return (
  //   <li className="item">
  //     { isPacked ? ( 
  //       <del>
  //         {name + ' ✔'}
  //       </del> 
  //     ) : ( name ) }
  //   </li>
  // )
}

function App() {
  return (
    <div className="App">
      <section>
        <h1>Sally Ride's Packing List</h1>
        <ul>
          <Item
            isPacked={true}
            name="Space suit"
          />
          <Item
            isPacked={true}
            name="Helmet with a golden leaf"
          />
          <Item
            isPacked={false}
            name="Photo of Tam"
          />
        </ul>
      </section>
    </div>
  );
}

export default App;
