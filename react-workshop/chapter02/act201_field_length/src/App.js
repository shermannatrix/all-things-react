import React, { Component } from "react";

class App extends Component {

	constructor (props) {
		super(props);
		this.state = { field: "", submitDisabled: true };
	}

	updateFieldLength(event) {
		const field = event.target.value;
		this.setState({ field }, () => {
			this.validateFieldLength();
		});
	}

	renderFieldLength() {
		return <p>{`${this.state.field.length} character(s)!`}</p>;
	}

	validateFieldLength() {
		if (this.state.submitDisabled && this.state.field.length > 100) {
			this.setState({ submitDisabled: false });
		} else if (this.state.submitDisabled && this.state.field.length <= 100) {
			this.setState({ submitDisabled: true });
		}
	}

	submitForm() {
		alert("Submitting the blog post.")
	}

	render() {
		return (
			<div className="App">
				<textarea 
					cols="80" 
					rows="25"
					onChange={this.updateFieldLength.bind(this)}></textarea>
				<br />
				{this.renderFieldLength()}
				<br />
				<button 
					disabled={this.state.submitDisabled}
					onClick={this.submitForm}>Submit Post</button>
			</div>
		);
	}
}

export default App;
