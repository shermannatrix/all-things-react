import React, { Component } from 'react';
import './App.css';

const TILE_COUNT = 24;

class App extends Component {
	constructor(props) {
		super(props);

		// Our initial state should have a black list of tiles, 
		// no previous flipped tile, and the no. of clicks for the
		// user starting at 0
		this.state = {
			tiles: [],
			lastFlipped: null,
			clicks: 0
		};

		this.resetTiles = this.resetTiles.bind(this);
		this.flipTile = this.flipTile.bind(this);
	}

	resetTiles() {
		// Start off with a blank tileset
		let tiles = [];
		// And start off with our numbering at 0
		let number = 0;
		// We're going to create two of the same tile for each number.
		for (let i = 0; i < TILE_COUNT; i += 2) {
			number++;
			// Create two tiles
			let tileOne = { flipped: true, matched: false, number };
			let tileTwo = { flipped: true, matched: false, number };

			// And add those to the list of tiles
			tiles = [...tiles, tileOne, tileTwo];
		}

		// Then randomize the tiles!
		for (let i = 0; i < tiles.length; i++) {
			// For each tile, pick a random one to switch it with
			const swapWith = Math.floor(Math.random() * tiles.length);
			// Swap the two tiles in place
			[tiles[i], tiles[swapWith]] = [tiles[swapWith], tiles[i]];
		}

		// Then update the state so the game starts over
		this.setState({ clicks: 0, tiles });
	}

	flipTile(index) {
		let tiles = this.state.tiles;

		let tile = tiles[index];

		let clicks = this.state.clicks + 1;

		let lastFlipped = this.state.lastFlipped;

		if (lastFlipped === null) {
			tiles = this.flipAllBackOver(tiles);
			tile.flipped = !tile.flipped;
			lastFlipped = index;
		} else {
			tile.flipped = !tile.flipped;
			let lastFlippedTile = this.state.tiles[lastFlipped];

			if (lastFlippedTile.number === tile.number) {
				lastFlippedTile.matched = true;
				tile.matched = true;
				tiles[lastFlipped] = lastFlippedTile;
			}

			// We've flipped two cards, so there is no more "previous" 
			// card to match against.
			lastFlipped = null;
		}

		// And update the current tile
		tiles[index] = tile;

		// And set the state, updating the number of clicks, the state of 
		// all of the tiles, and the last flipped tile's index.
		this.setState({ clicks, tiles, lastFlipped });
	}

	flipAllBackOver(tiles) {
		// For each tile, we want to see if it is matched. If it isn't,
		// we flip it back over.
		// Otherwise we leave it be.
		tiles.forEach(tile => {
			if (!tile.matched) {
				tile.flipped = true;
			}
		});
		return tiles;
	}

	renderTile(tile, index) {
		let classes = ["Tile"];
		if (tile.flipped) {
			classes = [...classes, "flipped"];
		}
		if (tile.matched) {
			classes = [...classes, "matched"];
		} 
		let key = `tile-${index}`;
		return (
			<div key={key} className={classes.join(" ")}
				onClick={() => this.flipTile(index)}>
				{!tile.flipped && tile.number}
			</div>
		);
	}

	render() {
		return (
			<div className="App">
				<h1>Memory Game</h1>
				<strong>Clicks: {this.state.clicks}</strong>
				<br />
				<button
					onClick={this.resetTiles} 
					className="reset">New Game</button>
				<hr />
				{ this.state.tiles.map((tile, index) => this.renderTile(tile, index)) }
			</div>
		);
	}
}

export default App;
