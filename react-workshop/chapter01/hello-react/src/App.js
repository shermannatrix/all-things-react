// import logo from './logo.svg';
import './App.css';

const Greeting = props => <p>Hello {props.name}!</p>;

const App = props => (
	<div>
		<h1>My App</h1>
		<br />
		<Greeting name="User" />
	</div>
);

export default App;
